<?php

use App\Kernel;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;

require dirname(__DIR__).'/vendor/autoload.php';

(new Dotenv())->bootEnv(dirname(__DIR__).'/.env');

function bootstrap(): void
{
    // $kernel = new \Tests\Functional\AppKernel('test', true);
    $kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
    $kernel->boot();

    $application = new Application($kernel);
    $application->setAutoExit(false);

    /**
     * INFO: https://symfony.com/doc/current/components/phpunit_bridge.html
     */

    $databaseIsCreated = $application->run(new ArrayInput([
        'command' => 'doctrine:database:create',
        '--quiet' => true,
    ]));

    /**
     * if $databaseIsCreated is 0, it means is the first test,
     * and we will load all tests fixtures
     */
    if ($databaseIsCreated === 0) {
        echo 'Database is created.' . PHP_EOL;

        $application->run(new ArrayInput([
            'command' => 'doctrine:schema:create',
        ]));

        $application->run(new ArrayInput([
            'command' => 'doctrine:fixtures:load',
            '-n' => true,
        ]));

        echo 'Fixtures is loaded.' . PHP_EOL;
    }

    $kernel->shutdown();
}

bootstrap();
