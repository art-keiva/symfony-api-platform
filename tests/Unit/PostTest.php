<?php

declare(strict_types=1);

namespace App\Tests\Unit;

use App\Entity\Post;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class PostTest extends TestCase
{
    private $post;

    protected function setUp(): void
    {
        parent::setUp();

        $this->post = new Post();
    }

    public function testGetTitle(): void
    {
        $value = 'Post title';

        $response = $this->post->setTitle($value);

        self::assertInstanceOf(Post::class, $response);
        self::assertSame($value, $this->post->getTitle());
    }

    public function testGetContent(): void
    {
        $value = 'Post content';

        $response = $this->post->setContent($value);

        self::assertInstanceOf(Post::class, $response);
        self::assertSame($value, $this->post->getContent());
    }

    public function testGetAuthor(): void
    {
        $value = new User();

        $response = $this->post->setAuthor($value);

        self::assertInstanceOf(Post::class, $response);
        self::assertInstanceOf(User::class, $this->post->getAuthor());
    }
}
