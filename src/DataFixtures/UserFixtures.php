<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker\Factory;

class UserFixtures extends Fixture
{
    public const ENTITY_REFERENCE = 'user';
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadEntities($manager);
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 0;

        foreach ($this->getEntityData() as [$email, $password, $roles, $createdAt]) {
            $entity = new User();

            $entity->setEmail($email)
                ->setPassword($this->passwordEncoder->encodePassword($entity, $password))
                ->setRoles($roles)
//                ->setCreatedAt($createdAt) // uncomment for fake data
            ;

            $manager->persist($entity);

            $this->addReference(self::ENTITY_REFERENCE . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function getEntityData(): array
    {
        $users = [
            // $userData = [$email, $password, $roles];
            ['john@example.com', 'john', ['ROLE_SUPER_ADMIN'], new \DateTime('now')],
            ['jane@example.com', 'jane', ['ROLE_ADMIN'], new \DateTime('now')],
        ];

        // fake users
        $fake = Factory::create();

        for ($i = 0; $i < 10; $i++) {
            $users[] = [
                $fake->email,
                'password',
                ['ROLE_USER'],
                new \DateTime('now + '.$i.'days'),
            ];
        }

        return $users;
    }
}
