<?php

namespace App\DataFixtures;

use App\Entity\Post;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class PostFixtures extends Fixture implements DependentFixtureInterface
{
    public const ENTITY_REFERENCE = 'post';

    public function load(ObjectManager $manager): void
    {
        $this->loadEntities($manager);
    }

    public function getDependencies()
    {
        return [ UserFixtures::class ];
    }

    private function loadEntities(ObjectManager $manager): void
    {
        $i = 0;

        foreach ($this->getEntityData() as [$title, $content, $createdAt, $author]) {
            $entity = new Post();

            $entity->setTitle($title)
                ->setContent($content)
//                ->setCreatedAt($createdAt) // uncomment for fake data
                ->setAuthor($author)
            ;

            $manager->persist($entity);

            $this->addReference(self::ENTITY_REFERENCE . $i, $entity);

            $i++;
        }

        $manager->flush();
    }

    private function getEntityData(): array
    {
        $posts = [];

        // fake posts
        $fake = Factory::create();

        $authors = $this->getAuthors();

        shuffle($authors);

        foreach ($authors as $i => $author) {
            $posts[] = [
                \str_replace('.', '', $fake->sentence(3)),
                $fake->paragraph,
                new \DateTime('now - '.$i.'days'),
                $author,
            ];
        }

        return $posts;
    }

    private function getAuthors(): array
    {
        $users = [];

        $i = 0;

        while ($this->hasReference($entityReference = UserFixtures::ENTITY_REFERENCE . $i++)) {
            $users[] = $this->getReference($entityReference);
        }

        krsort($users);

        return $users;
    }
}
