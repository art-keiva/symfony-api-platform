<?php

declare(strict_types=1);

namespace App\Events;

use ApiPlatform\Core\EventListener\EventPriorities;
use App\Authorizations\PostAuthorizationChecker;
use App\Entity\Post;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class PostSubscriber implements EventSubscriberInterface
{
    private $methodNotAllowed = [
        Request::METHOD_GET,
        Request::METHOD_POST,
    ];

    private $postAuthorizationChecker;

    public function __construct(PostAuthorizationChecker $postAuthorizationChecker)
    {
        $this->postAuthorizationChecker = $postAuthorizationChecker;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['check', EventPriorities::PRE_VALIDATE]
        ];
    }

    public function check(ViewEvent $event): void
    {
        $post = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if ($post instanceof Post &&
            !in_array($method, $this->methodNotAllowed, true)
        ) {
            $this->postAuthorizationChecker->check($post, $method);
            $post->setUpdatedAt(new \DateTimeImmutable());
        }
    }
}